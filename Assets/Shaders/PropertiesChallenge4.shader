﻿Shader "Holistic/PropChallenge4" 
{
    Properties {
        _DiffuseTex("Diffuse Texture", 2D) = "white" {}
		//set this texture to black to stop the white
	   //overwhelming the effect if no emission texture
	   //is present
		_EmissiveTex("Emissive Texture", 2D) = "black" {}
		_myRange("Example Range", Range(0,5)) = 1
    }
    SubShader {

      CGPROGRAM
        #pragma surface surf Lambert
        
        sampler2D _DiffuseTex;
		sampler2D _EmissiveTex;
		half _myRange;

        struct Input {
            float2 uv_DiffuseTex;
			float2 uv_EmissiveTex;
			
        };
        
        void surf (Input IN, inout SurfaceOutput o) {
            o.Albedo = (tex2D(_DiffuseTex, IN.uv_DiffuseTex)).rgb;
			o.Emission = (tex2D(_EmissiveTex, IN.uv_EmissiveTex)*_myRange).rgb;
        }
      
      ENDCG
    }
    Fallback "Diffuse"
  }
