﻿Shader "Holistic/PackedPractice" {
	
	Properties {
		_Colour ("Example Colour", Color) = (1,1,1,1)
	}

	SubShader {

		CGPROGRAM

			#pragma surface surf Lambert

			struct Input {
				float2 uvMainTex;
			};

			fixed4 _Colour;

			void surf (Input IN, inout SurfaceOutput o) {
				o.Albedo.xy = _Colour.rg;
			}

		ENDCG
	}
	FallBack "Diffuse"
}
